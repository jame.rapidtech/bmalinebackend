require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const moment = require("moment-timezone")
const _ = require("lodash")
const app = express();
const PORT = process.env.PORT;

const { v4: uuidv4 } = require("uuid");

const registration = require("./src/models/registration");
const member = require("./src/models/member");
const bma_menu = require("./src/models/bma_menu");

app.use(cors({ maxAge: 86400 }));
app.use(bodyParser.json());
app.options("*", cors());

app.post("/bma-line-manager/api/lineid", async (req, res, next) => {

  let { line_user_id, menu_code } = req.body;
  console.log("*****Start API lineid******")
  console.log("Line_user_id", line_user_id)
  const getRegistration = await registration.findAll({ where: {line_user_id, register_success: true} })
  console.log("getRegistration = ",getRegistration)

  console.log("Size getRegistration = ",_.size(getRegistration))

  if(_.size(getRegistration) === 0){
    line_user_id = false
    // menu_code = '0'
  } else {
    line_user_id = true
  }
  // console.log("member_registration -> ",getRegistration[0].dataValues.register_success)

  // for (let index = 0; index < getRegistration.length; index++) {
  //   const element = getRegistration[index].dataValues;
  //   console.log("++++++++ "+index+" ++++++++");
  //   console.log(element);
  //   console.log(element.id_card_number)
  // }
  console.log(menu_code)

  const geturl = await bma_menu.findAll({ where: {menu_code}})
  console.log("geturl = ", geturl)
  console.log("size geturl = ", _.size(geturl))
  console.log("---------------------------------------");

  let url = geturl

  if(_.size(geturl) === 0) {
    url = "notpage"
  } else {
    url = geturl[0].dataValues.menu_url
  }

  const data = {
    line_user_id: line_user_id,
    url: url
  }

  // console.log("Line Id ",getRegistration[0].dataValues.line_user_id)
  // res.send(getRegistration[0].dataValues.line_user_id)
  console.log("End API lineid")
  res.send(data);
});

app.post("/bma-line-manager/api/registrationfull", async (req, res, next) => {
  let body = req.body;

  const { id_card_number, birth_date , birth_year } = req.body;
  console.log("START*******************************************************")
  console.log("req.body = ",req.body)

  console.log('id_card_number =',id_card_number)
  console.log('birth_date =',birth_date)

  // console.log("State Date ")
// ------------------------------------------- เช็คเดท
  // const date = moment(`${birth_date} 00:00:00`, 'YYYY-MM-DD HH:mm:ss').utcOffset('-07:00').format()
  // console.log("date = ", date)
  // const date2 = moment(`${birth_date} 00:00:00`, 'YYYY-MM-DD HH:mm:ss').utcOffset('+07:00').format('YYYY-MM-DD HH:mm:ss')
  // console.log("date2 = ", date2)
  // const check_birth_date = await member.findAll({ 
  //   where: { 
  //     birth_date : date2
  //     // $gte:{birth_date:`${birth_date} 00:00:00`}, $lte:{birth_date:`${birth_date} 23:59:59`}
  //     // birth_date: {$gte:{birth_date:`${birth_date} 00:00:00`}, $lte:{birth_date:`${birth_date} 23:59:59`}}
  //     // birth_date: {$gte:{birth_date:` 00:00:00`}, $lte:{birth_date:`${birth_date} 23:59:59`}}
  //     },
  //   });
  // console.log("--------------------birth_date = ", check_birth_date);
// -------------------------------------------------


  // console.log("body ", body);
  // console.log("Idcard", req.body.id_card_number);
  // const id_card = req.body.id_card_number;
  // const birth_date = req.body.birth_date;
  console.log("------------ start Checkuser ------------")
 
  let checkuser = await member.findAll({ 
    where: { 
      // id_card: { $like: `%${id_card_number}`} , 
      id_card: id_card_number,
      birth_year
      },
    });

  console.log("CheckUser ", checkuser);
  console.log("------------ End Checkuser ------------")

  // if(_.size(checkuser)){
  //   console.log("checkuser[0] = ",checkuser[0].dataValues.birth_date)
  //   if(birth_date === _.toString(checkuser[0].dataValues.birth_date)){
  //     checkuser = true
  //   }else{
  //     checkuser = false
  //   }
  // } else {
  //   checkuser = false
  // }
  
  console.log("date_checkuser = ",checkuser)
  console.log("end Checkuser")
  console.log("----------------------------------------------")

  console.log("start check_id_card")
  let check_id_card = await registration.findAll({
    where: {
       id_card_number,
       
       register_success: true
    }
  })

  // if(_.size(check_id_card)){
  //   console.log("check_id_card[0] = ",check_id_card[0].dataValues.birth_date)
  //   if(birth_date === check_id_card[0].dataValues.birth_date){
  //     check_id_card = true
  //   } else {
  //     check_id_card = false
  //   }
  // }

  console.log("checkid_card = ",check_id_card)
  console.log("_.size(checkid_card) = ", _.size(check_id_card))
  console.log("end Checkid_card")
  console.log("----------------------------------------------")

  console.log("***************************************************  ");
  body = {
    member_id: uuidv4(),
    birth_date: req.body.birth_date,
    registered_date: moment().format('YYYY-MM-DD H:mm:ss'),
    id_card_number: req.body.id_card_number,
    line_photo: req.body.line_photo,
    line_user_id: req.body.line_user_id,
    line_email: req.body.line_email,
    line_display_name: req.body.line_display_name,
    ip: req.body.ip,
    register_success: _.size(checkuser) > 0 && _.size(check_id_card) === 0,
    deleted: false,
    accepted_pdpa: req.body.accepted_pdpa
  };console.log("dataOnSage",body);

  const createRegistration = await registration.create(body);
  console.log("SAVE ", createRegistration);

  // console.log(createRegistration)
  res.send(createRegistration);
});







app.post("/bma-line-manager/api/registration", async (req, res, next) => {
  let body = req.body;

  console.log("body ", body);

  const createRegistration = await registration.create(body);

  console.log(createRegistration);
  res.send(body);
});

app.post("/bma-line-manager/api/search"),
  async (req, res, next) => {
    const { id_card, birth_datess } = req.body;

    const search = await member.findAll({
      where: {
        id_card: { $like: `%${id_card}` },
        birth_date: birth_datess
      },
    });

    // console.log(search)
    res.send(search);
  };

// app.post('/bma-line-manager/api/member',async (req, res, next) => {
//   let body = req.body
//   console.log('body ', body)

//   const createRegistration = await member.create(body)

//   console.log(createRegistration)
//   res.send(body)

// })

app.get("/bma-line-manager/api/test", async (req, res, next) => {
  const uuid = uuidv4();
  res.send(uuid);
});

app.get("/Test", (req, res, next) => {
  res.send([10]);
});

require("./src/controllers/route")(app);

const server = app.listen(PORT, () => {
  console.info(`listening at ${server.address().port}`);
});
