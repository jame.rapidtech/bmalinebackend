require("dotenv").config();
const Sequelize = require("sequelize");
const {Op} = require('sequelize');

const operatorsAliases = {
    $like: Op.like,
    $iLike: Op.iLike,
    $or: Op.or,
    $and: Op.and,
    $not: Op.not,
    $in: Op.in,
    $gt: Op.gt,
    $lt: Op.lt,
    $gte: Op.gte,
    $lte: Op.lte,
    $eq: Op.eq,
    $notIn: Op.notIn,
  }
const sequelize = new Sequelize('BMA', process.env.user, process.env.pass, {
    host: process.env.host,
    port: process.env.portdatabase,
    dialect: 'postgres' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
    server:'postgres-db',
    define: {
        timestamps: true,
        freezeTableName: true,
        createdAt: false,
        updatedAt: false,
    },
    pool: {
        max: 1,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
    operatorsAliases
})

module.exports = sequelize;