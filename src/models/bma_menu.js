const Sequelize = require("sequelize")

const con = require("../../config/connect")

// models = con.define("table name ? , obj)",
const models = con.define("bma_menu",
  {
  menu_code:{
    // allowNull: false,
    primaryKey: true,
    type: Sequelize.STRING
  },
  menu_url:{
    
    type: Sequelize.DATE
  },
}

)
module.exports = models