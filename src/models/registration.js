const Sequelize = require("sequelize")

const con = require("../../config/connect")

// models = con.define("table name ? , obj)",
const models = con.define("member_registration",
  {
  member_id:{
    // allowNull: false,
    primaryKey: true,
    type: Sequelize.UUID
  },
  id_card_number: {
    type: Sequelize.STRING
  },
  line_photo: {
    
    type: Sequelize.STRING
  },
  line_user_id: {
    
    type: Sequelize.STRING
  },
  line_email: {
    
    type: Sequelize.STRING
  },
  line_display_name: {
    type: Sequelize.STRING
  },
  birth_date:{
    type: Sequelize.DATE
  },
  registered_date:{
    type: Sequelize.DATE
  },
  ip:{
    type: Sequelize.STRING
  },
  register_success:{
    type: Sequelize.BOOLEAN
  },
  created_date:{
    type: Sequelize.DATE
  },
  updated_date:{
    type: Sequelize.DATE
  },
  created_by:{
    type: Sequelize.UUID
  },
  updated_by:{
    type: Sequelize.UUID
  },
  deleted:{
    type: Sequelize.BOOLEAN
  },
  accepted_pdpa:{
    type: Sequelize.BOOLEAN
  },
}

)
module.exports = models