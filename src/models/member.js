const Sequelize = require("sequelize")

const con = require("../../config/connect")

// models = con.define("table name ? , obj)",
const models = con.define("member_profile",
  {
  member_id:{
    // allowNull: false,
    primaryKey: true,
    type: Sequelize.UUID
  },
  first_name:{
    
    type: Sequelize.STRING
  },
  last_name: {
   
    type: Sequelize.STRING
  },
  id_card: {
    
    type: Sequelize.STRING
  },
  birth_date: {
    
    type: Sequelize.DATE
  },
  member_position: {
    
    type: Sequelize.STRING
  },
  member_level: {
    
    type: Sequelize.STRING
  },
  member_type: {
    
    type: Sequelize.STRING
  },
  business_unit: {
    
    type: Sequelize.STRING
  },
  division: {
    
    type: Sequelize.STRING
  },
  department: {
    
    type: Sequelize.STRING
  },
  created_date: {
    
    type: Sequelize.DATE
  },
  birth_year: {
    
    type: Sequelize.STRING
  },
}

)
module.exports = models