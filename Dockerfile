FROM node:14.21

ENV APP_ENV product

RUN mkdir -p  /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

EXPOSE $PORT

ENTRYPOINT npm run start:$APP_ENV
